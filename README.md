# (RE)Sources Relationnelles - Projet CESI Alternance Cube 2023-2024

Bienvenue dans le projet **(RE)Sources Relationnelles** développé par une équipe de 4 personnes dans le cadre du programme Cesi Alternance Projet Cube. Ce projet est spécifiquement destiné au CESI du MANS pour l'année académique 2023-2024.

## Description du Projet
Le projet **(RE)Sources Relationnelles** vise à simuler une plateforme destinée aux citoyens, en réponse à une initiative portée par le Ministère des Solidarités et de la Santé. L'objectif est de fournir une plateforme complète regroupant des sources d'information, des ressources pratiques, et des espaces d'échanges pour favoriser la connectivité sociale.

## Branche du Projet
Le développement actif du projet se déroule sur la branche `master` du repository intitulé **ressources-mobile**. Vous pouvez suivre les dernières mises à jour et contributions sur cette branche.

## Collaborateurs
Ce projet est le fruit du travail collaboratif de quatre personnes passionnées et engagées.
Chacun a apporté ses compétences et son dévouement pour faire avancer le projet de manière significative.

*Projet développé dans le cadre de Cesi Alternance Projet Cube - Année académique 2023-2024.*